import type {ClassProvider, ExistingProvider, FactoryProvider, Type, ValueProvider} from "@nestjs/common";

/**
 * NestJS Provider generic type doesn't provide typechecking at all.
 * It is, according to NestJS maintainers, by design and to keep the framework flexible.
 *
 * We fix this issue by providing a flavor of the Provider type that is actually useful.
 *
 * @see https://github.com/nestjs/nest/issues/6522
 */
export declare type Provider<T = any> =
    Type<T>
    | ClassProvider<T>
    | ValueProvider<T>
    | FactoryProvider<T>
    | ExistingProvider<T>;
