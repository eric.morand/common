import type {ModuleMetadata} from "@nestjs/common";
import {Module as NestModule} from "@nestjs/common/decorators/modules/module.decorator";

/**
 * To instantiate modules, NestJS encourages the usage of classes exposing static functions, an approach which defeats
 * the whole purpose of Inversion of Control by forcing modules to know how to instantiate the modules they depend on.
 *
 * @Module({
 *     providers: [Connection],
 * })
 * export class DatabaseModule {
 *     static forRoot(entities = [], options?): DynamicModule {
 *         const providers = createDatabaseProviders(options, entities);
 *         return {
 *             module: DatabaseModule,
 *             providers: providers,
 *             exports: providers,
 *         };
 *     }
 * }
 *
 * NestJS also enforces modules that don't provide static functions to be empty shells.
 *
 * @Module({
 *     providers: [Connection],
 * })
 * class MainModule {}
 *
 * Here we expose a factory that solves both issues: by returning an anonymous module from a metadata object, it enforces proper
 * Inversion of Control, and by not being a Class Decorator, it doesn't force developers to write empty shells.
 *
 * const MainModule = Module({
 *     providers: [Connection],
 * });
 */
export const Module = (metadata: ModuleMetadata) => {
    const target = class {
    };

    NestModule(metadata)(target);

    return target;
};